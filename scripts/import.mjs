import path from "path";
import { API_ENDPOINT, FILES_DIR } from "../consts.js";
import { processData } from "../helpers/data.js";

const USERS_DIR = path.join(FILES_DIR, 'users');

const REQUEST_API_KEY = '8653d295-2aef-4c46-a9b4-ba270321c61c';
const REQUEST_URL = `${API_ENDPOINT}/users/track`;

const ATTRIBUTE_FIELDS = [
  'country',
  'email_subscribe',
  'email',
  'language',
  'push_subscribe',
  'time_zone',
];

const RATE_LIMITS = {
  batchSize: 100,
  quantity: 50000,
  seconds: 60,
};

const getId = (user) => {
  if (user.external_id) return { external_id: user.external_id };
  return { braze_id: user.braze_id };
}

const eventTransformer = (idObject) => (event) => ({
  ...idObject,
  name: event.name,
  time: event.last,
});

const importRequestTransformer = (fileName, fileData) => {
  // Attributes
  const attributes = fileData.map((user) => {
    const idObject = getId(user);

    // Create attributes object from multiple sources
    const attrs = { ...idObject, ...(user.custom_attributes || {}) };
    ATTRIBUTE_FIELDS.forEach((field) => {
      const value = user[field];
      if (value !== undefined) attrs[field] = value;
    })

    return attrs;
  })

  // Events
  const events = fileData
    .map((user) => {
      const idObject = getId(user);
      return (user.custom_events || []).map(eventTransformer(idObject));
    })
    .flat();

  return [
    REQUEST_URL,
    {
      method: 'post',
      headers: {
        Authorization: `Bearer ${REQUEST_API_KEY}`,
        'Content-Type': 'application/json',
        'X-Braze-Bulk': 'true',
      },
      body: JSON.stringify({
        attributes,
        events,
        purchases: [],
      }),
    },
  ];
};

// Imports user data back into Braze's global app group
// Uses JSON files containing data returned from the export script
const run = async () => {
  await processData(USERS_DIR, importRequestTransformer, RATE_LIMITS);
}

run();
