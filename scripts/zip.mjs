import path from "path";
import _ from 'lodash';
import { FILES_DIR } from "../consts.js";
import { mkdirSilent, readdir, readFile, writeFile } from '../helpers/file.js';

const ZIP_DIR = path.join(FILES_DIR, 'zip');

const MAX_USERS = 75;
const MAX_EVENTS = 75;

const getCustomEvents = (user) => user.custom_events || [];
const getChunkEvents = (chunk) => chunk.reduce((count, user) => count + getCustomEvents(user).length, 0);

// Extracts user export data into batch files ready for importing
const run = async () => {
  // Make a folder to output files into
  const chunksDirectory = path.join(ZIP_DIR, 'chunks');
  await mkdirSilent(chunksDirectory);

  // Get all files in the directory
  const allFileNames = await readdir(ZIP_DIR);
  const fileNames = allFileNames.filter((fileName) => fileName.endsWith('.txt'));

  // Read each text file
  for (const fileName of fileNames) {
    // Read each user
    const fileBuffer = await readFile(path.join(ZIP_DIR, fileName));
    const fileString = fileBuffer.toString();
    const lines = fileString.trim().split('\n').map((line) => JSON.parse(line.trim()));

    // Chunk users
    const allChunks = lines.reduce((chunks, line) => {
      const events = getCustomEvents(line).length;

      const latestChunk = chunks[chunks.length - 1];
      const chunkUsers = latestChunk.length;
      const chunkEvents = getChunkEvents(latestChunk);

      if (chunkUsers === MAX_USERS || chunkEvents + events > MAX_EVENTS) {
        // New chunk
        chunks.push([line]);
      } else {
        // Add to existing chunk
        latestChunk.push(line);
      }

      return chunks;
    }, [[]])

    // Write chunks
    const promises = allChunks.map((chunk, chunkIndex) => {
      const chunkFileName = `${fileName}-${chunkIndex + 1}.json`;
      const chunkFilePath = path.join(chunksDirectory, chunkFileName);

      console.info(`${chunkFileName}\tUsers: ${chunk.length}\tEvents: ${getChunkEvents(chunk)}`)

      return writeFile(chunkFilePath, JSON.stringify(chunk));
    });
    await Promise.allSettled(promises);
  }
}

run();
