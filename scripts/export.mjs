import path from "path";
import { API_ENDPOINT, FILES_DIR } from "../consts.js";
import { processData } from "../helpers/data.js";

/**
 * Example segment JSON file:
 * {
 *   "segment_id": "abc123",
 * }
 */

const SEGMENTS_DIR = path.join(FILES_DIR, 'segments');

const REQUEST_API_KEY = '782abdbf-3bc2-4b8b-afae-38145f301e17';
const REQUEST_URL = `${API_ENDPOINT}/users/export/segment`;
const REQUEST_FIELDS = [
  'apps',
  'braze_id',
  'country',
  'created_at',
  'custom_attributes',
  'custom_events',
  'email_subscribe',
  'email',
  'external_id',
  'language',
  'purchases',
  'push_subscribe',
  'total_revenue',
  'user_aliases',
  'time_zone',
]

const RATE_LIMITS = {
  batchSize: 1,
  quantity: 1000000,
  seconds: 1000000,
};

const exportRequestTransformer = (fileName, fileData) => {
  return [
    REQUEST_URL,
    {
      method: 'post',
      headers: {
        Authorization: `Bearer ${REQUEST_API_KEY}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        segment_id: fileData.segment_id,
        fields_to_export: REQUEST_FIELDS,
      }),
    },
  ];
}

// Request a ZIP download of all users in the given segments
const run = async () => {
  await processData(SEGMENTS_DIR, exportRequestTransformer, RATE_LIMITS);
};

run();
