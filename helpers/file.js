import fs from 'fs';
import util from 'util';

export const mkdir = util.promisify(fs.mkdir);
export const readdir = util.promisify(fs.readdir);
export const readFile = util.promisify(fs.readFile);
export const rename = util.promisify(fs.rename);
export const writeFile = util.promisify(fs.writeFile);

export const mkdirSilent = async (directory) => {
  try {
    await mkdir(directory);
  } catch (error) {
    if (!error.message.startsWith('EEXIST')) {
      console.warn(error.message);
    }
  }
}
