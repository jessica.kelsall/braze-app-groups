import _ from 'lodash';
import { DateTime } from 'luxon';
import path from 'path';
import { RATE_LIMIT_QUANTITY, RATE_LIMIT_SECONDS, REQUEST_BATCH_SIZE } from '../consts.js';
import { mkdirSilent, readdir, readFile, rename, writeFile } from './file.js';
import { pause } from './pause.js';
import { codeGroup, makeRequest } from './request.js';
import chalk from 'chalk';

const RATE_LIMIT_DEFAULTS = {
  batchSize: REQUEST_BATCH_SIZE,
  quantity: RATE_LIMIT_QUANTITY,
  seconds: RATE_LIMIT_SECONDS,
};

// Processes a single request JSON file
export const processFile = async (
  directories,
  fileName,
  requestTransformer,
  dryRun = true,
) => {
  // Read the file - JSON object
  const filePath = path.join(directories.base, fileName);
  const file = await readFile(filePath);
  const fileData = JSON.parse(file);

  // Prepare the request
  const [requestUrl, requestOptions] = requestTransformer(fileName, fileData);

  // Dry run: show details of what the request would be
  if (dryRun) {
    console.info(chalk.green(requestUrl));
    console.info(`${chalk.yellow('Headers:')}\t${JSON.stringify(requestOptions.headers)}`);
    console.info(`${chalk.yellow('Body:')}\t${requestOptions.body}`);
    return;
  }

  // Make the request
  const [responseStatus, responseBody] = await makeRequest(requestUrl, requestOptions);

  // Write the response to a file in a directory dependent on success
  const responseDirectory = codeGroup(responseStatus) === 200
    ? directories.responses : directories.errors;
  const responsePath = path.join(responseDirectory, fileName);
  await writeFile(responsePath, JSON.stringify(responseBody, null, 2));

  // Archive the original file
  const archivePath = path.join(directories.archive, fileName);
  await rename(filePath, archivePath);
};

// Process a directory of request JSON files
// Includes basic rate limiting and batching of requests
export const processData = async (
  directory,
  requestTransformer,
  rateLimits = RATE_LIMIT_DEFAULTS,
) => {
  // Make directories
  const directories = {
    base: directory,
    archive: path.join(directory, 'archive'),
    errors: path.join(directory, 'errors'),
    responses: path.join(directory, 'responses'),
  };
  await mkdirSilent(directories.archive);
  await mkdirSilent(directories.errors);
  await mkdirSilent(directories.responses);

  // Get all files in the given directory
  const allFileNames = await readdir(directory);
  const fileNames = allFileNames.filter((fileName) => fileName.endsWith('.json'));

  // Batch process the files
  // Only so many requests should be processing at once
  const fileBatches = _.chunk(fileNames, rateLimits.batchSize);
  const totalBatches = fileBatches.length;
  const batchesStarted = DateTime.now();

  // Impose rate limiting
  // When the limit is hit, wait until requests can be made again
  const getRateLimitEnd = (start) => DateTime.fromJSDate(start).plus({ seconds: rateLimits.seconds });
  let requestsMade = 0;
  let rateLimitStart = new Date();
  let rateLimitEnd = getRateLimitEnd(rateLimitStart);

  while (fileBatches.length > 0) {
    // Reset the rate limiter if its period elapses before the limit is hit
    if (rateLimitEnd <= new Date()) {
      requestsMade = 0;
      rateLimitStart = new Date();
      rateLimitEnd = getRateLimitEnd(rateLimitStart);
    }

    // If processing more requests would hit the rate limiter, wait for the next period
    if (requestsMade + fileBatches[0].length > rateLimits.quantity) {
      const thisPeriod = DateTime.fromJSDate(rateLimitStart)
      const nextPeriod = getRateLimitEnd(thisPeriod);
      const pauseMs = DateTime.now().diff(thisPeriod).as('milliseconds');

      const nextPeriodTime = nextPeriod.toFormat('HH:mm:ss');
      const nextPeriodRelative = nextPeriod.toRelative();
      console.info(`Rate limit hit, waiting until ${nextPeriodTime} (${nextPeriodRelative}).`);

      await pause(pauseMs);

    } else {
      // Process the batch

      // Report on progress so far
      const completedBatches = totalBatches - fileBatches.length;
      const percentageComplete = Math.floor((completedBatches / totalBatches) * 100);
      console.info(`Batch # ${completedBatches + 1}/${totalBatches} (${percentageComplete}% done).`);

      // Include an ETA
      if (completedBatches !== 0) {
        const msElapsed = DateTime.now().diff(batchesStarted).as('milliseconds');
        const msPerBatch = msElapsed / completedBatches;
        const totalMs = msPerBatch * totalBatches;
        const end = batchesStarted.plus({ milliseconds: totalMs });
        const endTime = end.toFormat('EEE d MMM HH:mm:ss');
        const endRelative = end.toRelative();

        console.info(`Estimated completion: ${endTime} (${endRelative}).`);
      }

      // Get the next batch and process each file in it
      const batch = fileBatches.shift();
      const promises = batch.map((fileName) => processFile(directories, fileName, requestTransformer));
      await Promise.allSettled(promises);
    }
  }

  console.info(`Completed: ${fileNames.length} files (${totalBatches} batches).`);
  console.info(`Started: ${batchesStarted.toRelative()}.`);
};
