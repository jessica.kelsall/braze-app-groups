import _ from 'lodash';
import { readFile } from "./file.js";

// Convert a CSV file to an array of objects with heading properties
export const parseCSV = async (filePath) => {
  const fileData = await readFile(filePath);

  // Convert the Buffer to a 2 dimensional array
  const [heading, ...lines] = fileData
    .toString()
    .trim()
    .split('\n')
    .map((line) => line.trim().split(','));

  // Convert each line into an object
  return lines.map((line) => _.zipObject(heading, line));
}
