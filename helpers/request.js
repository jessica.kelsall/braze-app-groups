import fetch from 'node-fetch';
import chalk from 'chalk';

export const codeGroup = (code) => Math.floor(code / 100) * 100;

export const formatHttpCode = (code) => {
  switch (codeGroup(code)) {
    case 100:
      return chalk.gray(code);
    case 200:
      return chalk.green(code);
    case 300:
      return chalk.blue(code);
    case 400:
      return chalk.yellow(code);
    case 500:
      return chalk.red(code);
  }
};

export const makeRequest = async (url, options) => {
  const response = await fetch(url, options);

  const body = await response.text();
  const jsonBody = JSON.parse(body);

  console.info('\t', formatHttpCode(response.status), url, body.length);

  return [response.status, jsonBody];
};
