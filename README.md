# braze-app-groups

## Purpose

Offline scripts for performing large volumes of Braze API requests. Designed specifically for migrating local app group users to the global app group.

## How it Works

Written in Node.js. Designed to be executed from the command line directly.

Each of the following subheadings is a `package.json` script, detailed in the order they need to be ran:

### CSV

- Requires a CSV file of user IDs exported from a Braze segment (use "ALL" segment).
- Pulls the `user_id` column and writes 50 at a time to sequentially numbered JSON files (e.g. `1.json`).

### Export

- Requires the JSON files created by the CSV script.
- Sends a Braze API request for each JSON file's list of user IDs (limited to 50 at a time by Braze).
- Each API response body is written to a correspondingly named file, in an "responses" directory.
- Any non 2XX HTTP response codes are instead written to an "errors" directory.
- The original files are moved into an "archive" directory for safe keeping.
  - If the script is interrupted at any point, this allows a re-run to pick up where it left off.
- API requests are processed in batches of 50 (this is how many requests can be sent at once).
- Info about progress and estimated completion time is provided as it goes.

### Import

- Requires the JSON files created by the Export script.
- Imports user information into the Braze global app group.
- Works in the same manner as the Export script described above.

## Setup

- Works with Node.js 14+,
- `npm i` to install dependencies.
- `package.json` scripts to run.

## TODO

Both the Export and Import scripts are missing their "request transformers" - these functions turn the contents of each file into a URL and [`node-fetch`](https://www.npmjs.com/package/node-fetch) options object (which includes HTTP verb and request body).

Line 7 in `scripts/export.mjs` and `scripts/import.mjs`.

Comments and boilerplate should make it clear what data is passed to each function, and what format the return value should be in.
