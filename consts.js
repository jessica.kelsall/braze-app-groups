// Local directories and files
export const CSV_FILE_NAME = 'ALL_export.csv';
export const FILES_DIR = '/Users/jkelsall/Documents/Braze/Automation';

// Rate limiting
export const REQUEST_BATCH_SIZE = 50; // How many API requests to run concurrently.
export const RATE_LIMIT_SECONDS = 3600; // How many seconds the rate limit applies to (... per X seconds).
export const RATE_LIMIT_QUANTITY = 2500000; // The limit for the rate limit period (Y requests per ...).

// Braze
export const API_ENDPOINT = 'https://rest.fra-01.braze.eu';
